package ch.hevs.aislab;

import ch.hevs.aislab.domain.*;
import ch.hevs.aislab.repository.PageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ExampleBackendApplication {

	private static final Logger log = LoggerFactory.getLogger(ExampleBackendApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ExampleBackendApplication.class);
	}

  @Bean
  public CommandLineRunner demoPage(PageRepository pageRepository)
  {
    return (args) -> {
      //save a couple pages
      //Page page1 = new Page("Page 1", "Pagina 1", "<p>Something in French here. <p>&lt;video=&#34;vid3&#34;&gt</p></p>", "<p>Something in Italian here.</p>");
      //Page page2 = new Page("Page 2", "Pagina 2", "<p>Something in French here.</p>", "<p>Something in Italian here.</p>");
      //Page page3 = new Page("Page 3", "Pagina 3", "<p>Something in French here.</p>", "<p>&lt;video=&#34;vid1&#34;&gt</p><p>Something in Italian here.</p>");

      //pageRepository.save(page1);
      //pageRepository.save(page2);
      //pageRepository.save(page3);
    };
  }
}
