package ch.hevs.aislab.controller;

/**
 * Created by yannp on 04.04.2016.
 */

import ch.hevs.aislab.domain.Page;
import ch.hevs.aislab.repository.PageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/v1/pages")
public class PageController {

  private static final Logger log = LoggerFactory.getLogger(PageController.class);
  private int pages_version = 0;

  @Autowired
  PageRepository pageRepository;

  // DEVELOP: change to localhost:8080
  @CrossOrigin(origins = "http://vlheresamont.hevs.ch:8080")
  @RequestMapping(method = RequestMethod.GET, value = "/version")
  public int getVersion() {
    log.info("get pages version");

    return this.pages_version;
  }

  // DEVELOP: change to localhost:8080
  @CrossOrigin(origins = "http://vlheresamont.hevs.ch:8080")
  @RequestMapping(method = RequestMethod.GET)
  public List<Page> getAll() {
    log.info("find all pages");
    return pageRepository.findAll();
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{idPage}")
  public Page get(@PathVariable long idPage){
    return pageRepository.findOne(idPage);
  }

  @RequestMapping(method = RequestMethod.POST)
  public Long insert(@RequestBody Page page) {
    log.info("save page " + page.toString());
    Page inserted = pageRepository.save(page);
    this.pages_version++;

    return inserted.getId();
  }


  @RequestMapping(method = RequestMethod.DELETE, value="/{idPage}" )
  public boolean delete(@PathVariable long idPage){
    log.info("delete page "+idPage);
    pageRepository.delete(idPage);
    this.pages_version++;

    return true;
  }

}
