package ch.hevs.aislab.domain;

/**
 * Created by yannp on 04.04.2016.
 */


  import javax.persistence.*;
  import java.util.ArrayList;
  import java.util.List;

@Entity
public class Page {


  @Id
  @GeneratedValue(strategy= GenerationType.AUTO)
  private long id;
  private String frenchName;
  private String italianName;

  @Column(columnDefinition = "LONGTEXT")
  private String frenchContent;
  @Column(columnDefinition = "LONGTEXT")
  private String italianContent;

  public Page()
  {}

  public Page(String frenchName, String italianName, String frenchContent, String italianContent) {
    this.frenchName = frenchName;
    this.italianName = italianName;
    this.frenchContent = frenchContent;
    this.italianContent = italianContent;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getFrenchName() {
    return frenchName;
  }

  public void setFrenchName(String frenchName) {
    this.frenchName = frenchName;
  }

  public String getItalianName() {
    return italianName;
  }

  public void setItalianName(String italianName) {
    this.italianName = italianName;
  }

  public String getFrenchContent() {
    return frenchContent;
  }

  public void setFrenchContent(String frenchContent) {
    this.frenchContent = frenchContent;
  }

  public String getItalianContent() {
    return italianContent;
  }

  public void setItalianContent(String italianContent) {
    this.italianContent = italianContent;
  }




  @Override
  public String toString() {
    return String.format(
      "Page[id=%d, frenchName='%s', italianName='%s']",
      id, frenchName, italianName);
  }
}
