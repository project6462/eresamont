package ch.hevs.aislab.repository;

/**
 * Created by yannp on 04.04.2016.
 */

import ch.hevs.aislab.domain.Page;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PageRepository extends CrudRepository<Page,Long>{

  List<Page> findAll();

  Page findOne(Long id);

  Page save(Page page);

  void delete(Page page);

}
