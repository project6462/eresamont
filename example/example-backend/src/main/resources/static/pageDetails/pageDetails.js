angular.module('eresamontBackend.pageDetails', [])
  .controller('pageDetailsCtrl', [ 'pageDetailsService','$route',
    function (pageDetailsService, $route) {
      //trick to access this in sub scope
      var self = this;

        var url = window.location.href;
        var id = url.substring(url.lastIndexOf('=') + 1);

        console.log(pageDetailsService.getInfo(id)
            .then(function (response) {
                self.page = response.data;
                pageDetailsService.myPage = self.page;
            }, function (error) {
                console.error(error);
            }));

        //save the text in the database
        self.saveText = function () {
          pageDetailsService.saveText(self.page);
          document.getElementById("clickSave").disabled = true;
          document.getElementById("clickSave").textContent = "No changes";
        }

        //activate the button to save
        self.modified = function () {
          document.getElementById("clickSave").disabled = false;
          document.getElementById("clickSave").textContent = "Save";
        }
    }])



  /**
   * Modify the text angular plugin to ower needs. We can select the functions we want to display and
   * add our own implementation.
   */
  .config(['$provide', function ($provide) {
    // we need just one listener for uploading new images
    var isListenerSet = false;

    // config the textAngular toolbar - specify the functions that should be provided
    $provide.decorator('taOptions', ['$delegate', function (taOptions) {
      taOptions.toolbar = [
        ['h1', 'h2', 'h3', 'p', 'pre', 'quote', 'justifyLeft', 'justifyCenter', 'justifyRight', 'html'],
        ['bold', 'italics', 'underline', 'ul', 'ol', 'redo', 'undo', 'clear', 'insertImage']
      ];

      return taOptions;
    }]);


    // add a custom button for the image upload
    $provide.decorator('taOptions', ['taRegisterTool', '$delegate', '$q',
      function (taRegisterTool, taOptions,   $q) {
        // $delegate is the taOptions we are decorating
        // register the tool with textAngular
        taRegisterTool('aisImage', {
          iconclass: "fa fa-image",
          action: function () {
            var self = this;

            //function to insert a new image to the rich text
            var insertListener = function () {
              isListenerSet = true;

              var file = document.querySelector('input[type=file]').files[0];
              var reader = new FileReader();

              // wait until the image is loaded
              reader.onloadend = function () {
                //build the html element
                var imageHtml = '<img src="' + reader.result + '" alt="Image not found">';

                //append the builded html element to the text
                self.$editor().wrapSelection('inserthtml', imageHtml);
              };

              // read the file as base64
              if (file) {
                reader.readAsDataURL(file);
              }
            };

            //click on the hidden input for selecting the file
            document.getElementById('fileWrapper').click();

            //set up a new listener if the listener is not already defined
            if (!isListenerSet) {
              document.getElementById('fileWrapper').addEventListener("change", insertListener);
            }
          }
        });

        //custom button for video
        taRegisterTool('aisVideo', {
          iconclass: "fa fa-video-camera",
          action: function () {
            var self = this;
              isListenerSet = true;

              //build the html element
              //Cannot insert it at once because of filter
              self.$editor().wrapSelection('inserthtml', '<')
              var idVid = document.getElementById('selectVid').value;

              var content = 'video="'+idVid+'">';

              //append the builded html element to the text
              self.$editor().wrapSelection('inserthtml', content);
          }
        });

        // add the button to the default toolbar definition
        taOptions.toolbar[1].push('aisImage');
        taOptions.toolbar[1].push('aisVideo');
        return taOptions;
      }]);
  }]);
