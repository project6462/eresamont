angular.module('eresamontBackend.pages', [])
  .controller('PagesCtrl', ['pagesService', '$route',
    function (pagesService, $route) {
      //trick to access this in sub scope
      var self = this;

      console.log(pagesService.getAll()
          .then(function (response) {
            self.pages = response.data;
          }, function (error) {
            console.error(error);
          }));

      self.add = function () {

        var copiedObject = jQuery.extend({}, self.page);

        //stop the add if the field is empty
        if (copiedObject.frenchName == undefined || copiedObject.frenchName == "" || copiedObject.italianName == undefined || copiedObject.italianName == "") {
          document.getElementById("errorLabel").innerHTML = "Name field is empty";
          return;
        }


        //clear error message if there is one
        document.getElementById("errorLabel").innerHTML = "";

        pagesService.add(copiedObject)
            .then(function (response) {
              copiedObject.id = response.data;
            }, function (error) {
              console.error(error);
            });

        self.pages.push(copiedObject);

        self.page.frenchName = "";
        self.page.italianName = "";
      }


      //function to use when user delete a page
      self.deleteOne = function (page) {
        if(confirm("Do you really want to delete page "+page.frenchName))
        {
          self.pages.splice(self.pages.indexOf(page),1);
          pagesService.deleteOne(page.id);
        }
      }


      //function to manage the locker on the fields
      self.lockTxt = function (page) {
        //get the text field
        var elementFr = document.getElementById("inputTextDesign" + page.id);
        var elementIt = document.getElementById("inputTextDesignIt" + page.id);

        //test if we need to open or to close the field
        if (elementFr.disabled && elementIt.disabled) {
          //unlock the field
          document.getElementById("imgLock" + page.id).src = "img/open.png";
          elementFr.disabled = false;
          elementIt.disabled = false;
        }
        else {
          //lock the field if the modification can be made
          if (elementFr.value == undefined || elementFr.value == "" || elementIt.value == undefined || elementIt.value == "") {
            //stop the add if the field is empty
            document.getElementById("errorLabel").innerHTML = "Name field is empty";
            throw new Error("Field is empty");
          }
          if (elementFr.value.indexOf("/") != -1 || elementIt.value.indexOf("/") != -1) {
            //stop the add if the user try to put unauthorized char
            document.getElementById("errorLabel").innerHTML = "/ is not authorized";
            throw new Error("/ is not authorized");
          }

          //clear error message if there is one
          document.getElementById("errorLabel").innerHTML = "";

          //save modification
          page.frenchName = elementFr.value;
          page.italianName = elementIt.value;
          pagesService.add(page);

          //relock the field
          document.getElementById("imgLock" + page.id).src = "img/close.png";
          elementFr.disabled = true;
          elementIt.disabled = true;

        }

      }

    }]);
