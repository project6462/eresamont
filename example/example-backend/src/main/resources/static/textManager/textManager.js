angular.module('eresamontBackend.textManagers', [])
    .controller('textManagerCtrl', ['pageDetailsService', '$route',
        function (pageDetailsService, $route) {
            //trick to access this in sub scope
            var self = this;

            //PROBLEM HERE

                if(pageDetailsService.myTextId != undefined)
                {
                    alert("AVANT : "  + self.article.contentFrench);
                    self.article.contentFrench = pageDetailsService.myTextId.contentFrench ;
                    self.article.contentItalian = pageDetailsService.myTextId.contentItalian ;
                    alert("APRES : "  + self.article.contentFrench);
                    pageDetailsService.myTextId = undefined;
                }


            self.add = function () {

                var copiedObject = jQuery.extend({}, self.article);



                //stop the add if the field is empty
                alert("COPIEOBJ : " + copiedObject.contentFrench + "\n" + "COPIEOBJIT : " + copiedObject.contentItalian);
                if (copiedObject.contentFrench == undefined || copiedObject.contentFrench == "" || copiedObject.contentItalian == undefined || copiedObject.contentItalian == "") {
                    document.getElementById("errorLabel").innerHTML = "Name field is empty";
                    return;
                }

                //clear error message if there is one
                document.getElementById("errorLabel").innerHTML = "";

                copiedObject.page = pageDetailsService.myPage;

                pageDetailsService.add(copiedObject)
                    .then(function (response) {
                        copiedObject.id = response.data;
                    }, function (error) {
                        console.error(error);
                    });

                self.article.contentFrench = "";
                self.article.contentItalian = "";
            }


}]);
