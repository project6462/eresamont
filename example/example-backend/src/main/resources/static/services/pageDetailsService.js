angular.module('eresamontBackend.pageDetailsService', [])
  .service('pageDetailsService', ['$http', '$q',
    function ($http) {

        var myPage;
        var self = this;
        var myTextId ;


      this.getInfo = function (idPage) {
        return $http.get("/api/v1/pages/"+idPage);
      };
      
      this.saveText = function (page) {
        return $http.post("api/v1/pages", page)
      }

      this.deleteArticle = function (idArticle) {
        return $http.delete("/api/v1/texts/"+idArticle);
      }

        this.add = function(article){
            article.page = self.myPage;
            return $http.post("/api/v1/texts/", article);
        };

    }]);
