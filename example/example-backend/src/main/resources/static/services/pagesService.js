angular.module('eresamontBackend.pagesService', [])
  .service('pagesService', ['$http', '$q',
    function ($http) {

      this.getAll = function()
      {
        return $http.get("/api/v1/pages");
      };

      this.getByName = function (frenchName)
      {
        return $http.get("/api/v1/pages/"+frenchName);
      };

      this.add = function(page){
        return $http.post("/api/v1/pages", page);
      };

      this.deleteOne = function (page) {
        $http.delete("/api/v1/pages/"+page);
      };


    }]);
