angular.module('eresamontBackend', [
        'textAngular',
        'ngRoute',
        'eresamontBackend.pages',
        'eresamontBackend.pagesService',
        'eresamontBackend.pageDetails',
        'eresamontBackend.pageDetailsService',
        'eresamontBackend.textManagers'

    ])
    //routing for the navigation
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider
          .when('/about',{templateUrl:'about/about.html'})
          .when('/pages',{
            templateUrl:'pages/pages.html',
            controller:'PagesCtrl',
            controllerAs:'pc'
          })
          .when('/pageDetailsFR',{
            templateUrl:'pageDetails/pageDetailsFR.html',
            controller:'pageDetailsCtrl',
            controllerAs:'pdc'
          })
          .when('/pageDetailsIT',{
            templateUrl:'pageDetails/pageDetailsIT.html',
            controller:'pageDetailsCtrl',
            controllerAs:'pdc'
          })



    }]);




