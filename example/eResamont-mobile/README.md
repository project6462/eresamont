# Environment installation:
npm install -g bower cordova ios-deploy

# Project dependencies installation:
cordova platform install
cordova plugin install
bower install

# Check what is missing for the project to run on one or more platforms:
cordova requirements

# Running project locally:
cordova run browser

# Running project in emulators:
cordova emulate android
cordova emulate ios

# Running project on devices (after connecting them):
cordova run android
cordova run ios

# Security
Security-related options that need adjustments in production can be found by a "FIXME" comments in the project.

# Development
To work in development-only environment, find strings starting with "DEVELOP" comment and follow instructions.

# Additional resources about full development environment installation:
https://cordova.apache.org/docs/en/latest/guide/platforms/ios/index.html - how to setup iOS deployment environment
https://cordova.apache.org/docs/en/latest/guide/platforms/android/index.html - how to setup Android deployment environment
http://www.wastedpotential.com/running-xcode-projects-on-a-device-without-a-developer-account-in-xcode-7/ - deploying to iOS without developer account
