"use strict";

angular.module("ngapp").controller("MainController", function(shared, $state, $scope, $mdSidenav, $mdComponentRegistry){

    var ctrl = this;

    this.auth = shared.info.auth;

    this.language = shared.language;
    this.title = $state.current.title;

    shared.getAll()
        .then(function (data) {
            ctrl.pages = data;
        }, function (error) {
            console.error(error);
        });

    this.goToArticle = function (id) {
        shared.articleID = id;

        window.location = "#/article";
    };

    this.changeLanguage = shared.changeLanguage;
});
