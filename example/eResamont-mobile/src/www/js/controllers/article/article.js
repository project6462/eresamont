"use strict";

angular.module("ngapp").controller("articleController", function(shared,$sce, $state, $scope, $mdSidenav, $mdComponentRegistry){

    var self = this;

    this.auth = shared.info.auth;

    this.language = shared.language;
    this.title;
    this.content;

    if (shared.articleID == undefined) {
        window.location = '#/main';
    }

    shared.getAll()
        .then(function (data) {
            var page = self.getPageById(data, shared.articleID);

            self.title = page[self.language + 'Name'];
            self.content = page[self.language + 'Content'];

            for (var pos_start_video = self.content.indexOf('<p>&lt;video=&#34;'); pos_start_video != -1; pos_start_video = self.content.indexOf('<p>&lt;video=&#34;')) {
                var pos_end_video = self.content.indexOf('&#34;&gt');

                // DEVELOP: change to localhost:8080
                self.content = self.content.substring(0, pos_start_video) +
                               '<video width="100%" controls><source src="http://vlheresamont.hevs.ch:8080/video/' + self.content.substring(pos_start_video + 18, pos_end_video) + '.mp4" type="video/mp4">' +
                               'Your mobile does not support HTML5 video.' +
                               '</video>' +
                               self.content.substring(pos_end_video + 8);
            }

            self.content = $sce.trustAsHtml(self.content);
        }, function (error) {
            console.error(error);
        });


    this.getPageById = function(pages, page_id) {
        for (var i = 0, pages_count = pages.length; i < pages_count; i++) {
            if (pages[i].id == page_id) {
                return pages[i];
            }
        }
    }

    this.changeLanguage = shared.changeLanguage;
});
