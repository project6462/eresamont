"use strict";

 // ngTouch is No Longer Supported by Angular-Material
angular.module("ngapp", [ "ngTouch", "ui.router", "ngMdIcons", "ngMaterial", "ngCordova", "ngStorage"])
    .run(function($rootScope, $cordovaDevice, $cordovaStatusbar){
        document.addEventListener("deviceready", function () {
            $cordovaStatusbar.overlaysWebView(false); // Always Show Status Bar
            $cordovaStatusbar.styleHex('#E53935'); // Status Bar With Red Color, Using Angular-Material Style
            window.plugins.orientationLock.lock("portrait");
        }, false);
    })
    .config(function($mdThemingProvider) { // Angular-Material Color Theming
        $mdThemingProvider.theme('default')
            .primaryPalette('red')
            .accentPalette('blue');
    });
