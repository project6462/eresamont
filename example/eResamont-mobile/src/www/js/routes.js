"use strict";

angular.module("ngapp").config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider){

    $urlRouterProvider
        .otherwise("/main");

    $stateProvider
        .state("article", {
            url: "/article",
            templateUrl: "js/controllers/article/article.html",
            title: "eResamont",
            controller: "articleController",
            controllerAs: "article"
        })
        .state("main", {
            url: "/main",
            templateUrl: "js/controllers/main/main.html",
            title: "eResamont",
            controller: "MainController",
            controllerAs: "main"
        });
}]);
