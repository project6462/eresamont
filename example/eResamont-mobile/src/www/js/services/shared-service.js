"use strict";

angular.module("ngapp").service("shared", ["$http", "$q",function($http){ // One of The Ways To Share Informations Across the Controllers

    this.info = {
        title: "eResamont",
        auth: "NTechnologies"
    };

    this.language = (window.localStorage.getItem('language') || 'french');

    this.getAll = function() {
        var storage = window.localStorage;

        var pages_version = storage.getItem('pages_version');

        // DEVELOP: change to localhost:8080
        return $http.get('http://vlheresamont.hevs.ch:8080/api/v1/pages/version').then(function (response) {
            var current_version = response.data;

            if (pages_version != current_version) {
                // DEVELOP: change to localhost:8080
                $http.get('http://vlheresamont.hevs.ch:8080/api/v1/pages').then(function (response) {
                    storage.setItem('pages', JSON.stringify(response.data));
                    storage.setItem('pages_version', current_version);
                }, function (error) {
                    console.error(error);
                });
            }
        }, function (error) {
            console.error(error);
        }).then(function () {
            var pages_string = storage.getItem('pages');

            return new Promise(function(resolve, reject) {
                if (pages_string) {
                    resolve(JSON.parse(pages_string));
                } else {
                    reject(Error('Pages\' content doesn\'t appear in the localStorage.'));
                }
            });
        });
    };

    this.changeLanguage = function(language) {
        var storage = window.localStorage;

        storage.setItem('language', language);

        window.location.reload();
    };
}]);
